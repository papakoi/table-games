using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PapaKoi.TableGames;

namespace TableGamesTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test52CardDeck()
        {
            List<PlayingCard> deck = PlayingCard.CreateDeck();
            Assert.AreEqual(deck.Count, 52);
        }

        [TestMethod]
        public void TestDrawFive()
        {
            List<PlayingCard> deck = PlayingCard.CreateDeck();
            PlayingCard sixthCard = deck[5];
            int handCount = deck.Draw(5).Count;
            Assert.AreEqual(handCount, 5);
        }

        [TestMethod]
        public void Test47CardDeckAfterDrawFive()
        {
            List<PlayingCard> deck = PlayingCard.CreateDeck();
            PlayingCard sixthCard = deck[5];
            int startCount = deck.Count;
            int handCount = deck.Draw(5).Count;
            int endCount = deck.Count;
            Assert.AreEqual(endCount, startCount - handCount);
        }

        [TestMethod]
        public void TestSixthCardIsFirstCardAfterDrawFive()
        {
            List<PlayingCard> deck = PlayingCard.CreateDeck();
            PlayingCard sixthCard = deck[5];
            deck.Draw(5);
            Assert.AreEqual(sixthCard, deck[0]);
        }

        [TestMethod]
        public void TestFiveCoinFlips()
        {
            Assert.AreEqual(DiceAndCoins.FlipCoins(random: _rng, count: 5).Count(), 5);
        }

        [TestMethod]
        public void TestFiveDiceRolls()
        {
            List<PlayingCard> deck = PlayingCard.CreateDeck();
            Assert.AreEqual(DiceAndCoins.RollDice(random: _rng, count: 5, sides: 6).Count(), 5);
        }

        private Random _rng = new();
    }
}
