# Table Games Test

To run the unit tests, use the following command in the root folder of this repository:

```
dotnet test TableGamesTest/TableGamesTest.csproj
```
