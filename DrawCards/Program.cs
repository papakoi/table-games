﻿using System;
using System.Collections.Generic;
using PapaKoi.TableGames;

namespace DrawCards
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 0;

            while (true)
            {
                try
                {
                    Console.Write("Enter the number of cards to draw: ");
                    string input = Console.ReadLine();
                    count = int.Parse(input);
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Must enter a positive integer. Try Again.");
                }
            }

            List<PlayingCard> deck = PlayingCard.CreateDeck();
            Random rng = new();
            deck.Shuffle(rng);
            foreach (var card in deck.Draw(count))
            {
                Console.WriteLine(card);
            }
        }
    }
}
