﻿using System;
using PapaKoi.TableGames;

namespace FlipCoins
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 0;

            while (true)
            {
                try
                {
                    Console.Write("Enter the number of coins to flip: ");
                    string input = Console.ReadLine();
                    count = int.Parse(input);
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Must enter a positive integer. Try Again.");
                }
            }

            Random rng = new();
            foreach (var coin in DiceAndCoins.FlipCoins(rng))
            {
                Console.WriteLine(coin);
            }
        }
    }
}
