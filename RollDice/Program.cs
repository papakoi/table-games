﻿using System;
using PapaKoi.TableGames;

namespace RollDice
{
    class Program
    {
        static void Main(string[] args)
        {
            var sides = 0;
            var count = 0;

            while (true)
            {
                try
                {
                    string input = "";

                    if (sides == 0)
                    {
                        Console.Write("Enter the number of sides for each die: ");
                        input = Console.ReadLine();
                        sides = int.Parse(input);
                    }

                    Console.Write("Enter the number of dice to roll: ");
                    input = Console.ReadLine();
                    count = int.Parse(input);
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Must enter a positive integer. Try Again.");
                }
            }

            Random rng = new();
            foreach (var die in DiceAndCoins.RollDice(rng, count, sides))
            {
                Console.WriteLine(die);
            }
        }
    }
}
