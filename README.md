# Table Games

A library of simple table game logic that I've needed in a few places. The
solution includes a few interactive test programs in addition to the supplied
unit tests. This was created as a learning exercise and does not represent
current development best practices.

## Build Instructions

If you just need the DLLs, run the `dotnet build` command:

```
dotnet build
```

To create the NuGet package, use `dotnet pack`:

```
dotnet pack TableGames/TableGames.csproj
```

## Playing Cards

Be sure to add the root namespace into your `using` directives.

```csharp
using PapaKoi.TableGames;
```

This library can create a basic representation of the [standard 52-card
deck](https://en.wikipedia.org/wiki/Standard_52-card_deck).

```csharp
List<PlayingCard> deck = PlayingCard.CreateDeck();
```

Add additional decks in order to create large decks.

```csharp
deck.AddRange(PlayingCard.CreateDeck());
deck.AddRange(PlayingCard.CreateDeck());
deck.AddRange(PlayingCard.CreateDeck());
```

A deck can be shuffled in-place using `Random`.

```csharp
Random rng = new();
deck.Shuffle(rng)
```

---

**NOTE**

Any `IList<T>` can be shuffled using this method.

---

Hands can be drawn for as long as there are cards in the deck. Cards are drawn
from the start of the list, so consider the start of the list to be the top of
the deck. Attempting to draw from an empty deck returns an empty hand.

```csharp
List<PlayingCard> hand1 = deck.Draw(5);
List<PlayingCard> hand2 = deck.Draw(5);
```

Returning hands is a matter of adding them back into the deck. For example,
consider the following logic for a type of mulligan where the player returns
their hand to their deck, shuffles their deck, and draws a new hand before
having to return one additional card to the bottom of their deck as a penalty.

```csharp
Player player = new();

// initial hand
var mulligans = 0;
List<PlayingCard> hand = deck.Draw(7);

// mulligan
++mulligans;
deck.AddRange(hand);
deck.Shuffle(rng);
hand = deck.Draw(7);
List<PlayingCard> cardsToReturn = player.ChooseCards(hand, count: mulligans);
foreach (var card in cardsToReturn)
{
    hand.Remove(card);
}
deck.AddRange(cardsToReturn);
```

---

**NOTE**

The `Player` class shown here is merely an example and does not exist within
this library.

---

## Dice and Coins

To simulate coin flips, create a `Random` object, then pass that to the
`FlipCoins` method in addition to the amount of coins you want to flip.

```csharp
Random rng = new();
IEnumerable<PapaKoi.TableGames.CoinSide> coins =
    PapaKoi.TableGames.DiceAndCoins.FlipCoins(rng, count: 5);
foreach (var side in coins)
{
    Console.WriteLine(side);
}
```

Likewise, to simulate dice rolls, create a `Random` object, then pass that to
the `RollDice` method. The number of sides for the dice can be given if it needs
to be different from the typical six sides. The following example rolls 3 D20s.

```csharp
Random rng = new();
IEnumerable<int> dice =
    PapaKoi.TableGames.DiceAndCoins.RollDice(rng, count: 3, sides: 20);
foreach (var result in dice)
{
    Console.WriteLine(result);
}
```
