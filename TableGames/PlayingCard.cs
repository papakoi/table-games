﻿using System;
using System.Collections.Generic;

namespace PapaKoi.TableGames
{
    /// <summary>
    /// A playing card which has a rank and suit.
    /// </summary>
    public record PlayingCard(Rank Rank, Suit Suit)
    {
        /// <summary>
        /// Create and return a list containing the standard 52-card deck.
        /// </summary>
        public static List<PlayingCard> CreateDeck()
        {
            var deck = new List<PlayingCard>();
            var ranks = Enum.GetValues<Rank>();
            var suits = Enum.GetValues<Suit>();
            foreach (var rank in ranks)
            {
                foreach (var suit in suits)
                {
                    deck.Add(new PlayingCard(rank, suit));
                }
            }
            return deck;
        }

        public override string ToString()
        {
            return $"{Rank} of {Suit}";
        }
    }

    /// <summary>
    /// The standard 13 ranks for playing cards.
    /// </summary>
    public enum Rank
    {
        Ace, Two, Three, Four, Five, Six, Seven,
        Eight, Nine, Ten, Jack, Queen, King
    }

    /// <summary>
    /// The standard four suits for playing cards.
    /// </summary>
    public enum Suit
    {
        Clubs, Diamonds, Hearts, Spades
    }
}
