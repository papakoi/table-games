using System;
using System.Collections.Generic;

namespace PapaKoi.TableGames
{
    /// <summary>
    /// Simulate dice rolls and coin flips.
    /// </summary>
    public class DiceAndCoins
    {
        /// <summary>
        /// Flip some coins.
        /// </summary>
        /// <returns>
        /// A sequence of coin flips using a pseudo-random number generator.
        /// </returns>
        /// <param name="random">
        /// The pseudo-random number generator used to get the results.
        /// </param>
        /// <param name="count">
        /// The number of coins to flip. At least one coin must be flipped. Five
        /// coins are flippped by default.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// The pseudo-random number generator was null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Attempted to flip fewer than one coins.
        /// </exception>
        public static IEnumerable<CoinSide> FlipCoins(Random random, int count = 5)
        {
            if (random is null)
            {
                throw new ArgumentNullException(nameof(random));
            }
            if (count < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(count), count,
                    "Coin count was out of range. Must be a value greater than or equal to 1.");
            }

            for (int i = 0; i != count; ++i)
            {
                yield return (CoinSide)random.Next(2);
            }
        }

        /// <summary>
        /// Roll some dice where each die has a certain number of sides.
        /// </summary>
        /// <returns>
        /// A sequence of dice rolls using a pseudo-random number generator.
        /// </returns>
        /// <param name="random">
        /// The pseudo-random number generator used to get the results.
        /// </param>
        /// <param name="count">
        /// The number of dice to roll. At least one die must be rolled. Five
        /// dice are rolled by default.
        /// </param>
        /// <param name="sides">
        /// The number of sides for each die. Each die must have at least two
        /// sides.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// The pseudo-random number generator was null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Either the number of dice to roll was fewer than one or the number
        /// of sides for each die was less than two.
        /// </exception>
        public static IEnumerable<int> RollDice(Random random, int count = 5, int sides = 6)
        {
            if (random is null)
            {
                throw new ArgumentNullException(nameof(random));
            }
            if (count < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(count), count,
                    "Dice count was out of range. Must be a value greater than or equal to 1.");
            }
            if (sides < 2)
            {
                throw new ArgumentOutOfRangeException(nameof(sides), sides,
                    "Number of sides was out of range. Must be a value greater than or equal to 2.");
            }

            for (int i = 0; i != count; ++i)
            {
                yield return random.Next(sides) + 1;
            }
        }
    }

    /// <summary>
    /// The two sides of a coin.
    /// </summary>
    public enum CoinSide
    {
        Heads, Tails
    }
}