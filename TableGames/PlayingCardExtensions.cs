using System;
using System.Collections.Generic;
using System.Linq;

namespace PapaKoi.TableGames
{
    /// <summary>
    /// Extensions to use when working with playing cards.
    /// </summary>
    public static class PlayingCardExtensions
    {
        /// <summary>
        /// Shuffle the contents of the list using a pseudo-random number
        /// generator. The list is left untouched if there is only one item in
        /// the list or if the list is empty.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// Either <paramref name="source"/> or <paramref name="random"/> are
        /// null.
        /// </exception>
        public static void Shuffle<T>(this IList<T> source, Random random)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            if (random is null)
            {
                throw new ArgumentNullException(nameof(random));
            }
            if (source.Count < 2)
            {
                return;
            }

            for (int i = 0; i != source.Count; ++i)
            {
                int j = random.Next(i + 1);
                T temp = source[i];
                source[i] = source[j];
                source[j] = temp;
            }
        }

        /// <summary>
        /// Draw up to a given number of items from the list.
        /// </summary>
        /// <returns>
        /// A new list with up to <paramref name="count"/> items taken from the
        /// beginning of <paramref name="source"/> and removes those same items
        /// from <paramref name="source"/>. If the number of items remaining in
        /// <paramref name="source"/> is less than <paramref name="count"/>,
        /// then those items are returned as a new list and <paramref
        /// name="source"/> is cleared.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// The <paramref name="source"/> list is null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The number of items to draw is less than zero.
        /// </exception>
        public static List<T> Draw<T>(this IList<T> source, int count)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), count,
                    "The number of items to draw must be a positive integer or zero.");
            }

            if (source.Count < count)
            {
                var smallResult = new List<T>(source);
                source.Clear();
                return smallResult;
            }

            var result = source.Take(count).ToList();
            foreach (var item in result)
            {
                source.RemoveAt(0);
            }
            return result;
        }
    }
}
